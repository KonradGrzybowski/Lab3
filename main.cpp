#include <iostream>
#include <math.h>

using namespace std;

class MathString {
public:
    virtual double GetValue(int n)=0;
};

class AritmeticString :public MathString {
    public:
    float pierwszywyraz;
    float roznica;

    virtual double GetValue(int n){
     return pierwszywyraz+(n-1)*roznica;
    }

    AritmeticString (float pier=3, float r=5){
    pierwszywyraz=pier;
    roznica=r;
    }
};


class GeometricString :public MathString {
    public:
    float pierwszy;
    float iloraz;
    virtual double GetValue (int n){
        return pierwszy*pow (iloraz, n-1);
        }

    GeometricString (float pier=5, float ilor=2){
        pierwszy=pier;
        iloraz=ilor;
    }
};

class SubstringCalculator {

    public:
        MathString *obiekt;


        SubstringCalculator(MathString *wart){

            obiekt=wart;
        }


        double Calculate (int from, int to){
           double suma=0;
            for (int i=from; i<=to; i++){
                suma=suma+obiekt->GetValue(i);
            }
            return suma;
        }



};




int main()
{
    AritmeticString a1 (100,2);
    AritmeticString *wsk_a1 = &a1;
    GeometricString g1 (3,10);
    GeometricString *wsk_g1=&g1;
    SubstringCalculator asd(wsk_a1);
    SubstringCalculator asd1(wsk_g1);
    cout<<asd.Calculate(5,10)<<endl;
    cout<<asd1.Calculate(3,5);

}